<?php
/**
 * @file Default structures (Fields, etc.) exported by Features for the Ubercart Affiliate Product module.
 *
 * @author Guy Paddock (guy.paddock@redbottledesign.com).
 */

  /**
   * Helper to implementation of hook_content_default_fields().
   */
  function _uc_affiliate_product_content_default_fields() {
    $fields = array();

    if (module_exists('imagefield')) {
      // Exported field: field_image_cache
      $fields[] = array(
        'field_name' =>  UCAP_FIELD_NAME_IMAGE,
        'type_name' => UCAP_CONTENT_TYPE_AFFILIATE_PRODUCT,
        'display_settings' => array(
          'weight' => '-1',
          'parent' => '',
          'label' => array(
            'format' => 'hidden',
          ),
          'teaser' => array(
            'format' => 'hidden',
            'exclude' => 0,
          ),
          'full' => array(
            'format' => 'hidden',
            'exclude' => 0,
          ),
          '4' => array(
            'format' => 'hidden',
          ),
        ),
        'widget_active' => '1',
        'type' => 'filefield',
        'required' => '0',
        'multiple' => '1',
        'module' => 'filefield',
        'active' => '1',
        'list_field' => '0',
        'list_default' => 1,
        'description_field' => '0',
        'widget' => array(
          'file_extensions' => 'gif jpg png jpeg',
          'file_path' => '',
          'progress_indicator' => 'bar',
          'max_filesize_per_file' => '',
          'max_filesize_per_node' => '',
          'max_resolution' => 0,
          'min_resolution' => 0,
          'alt' => '',
          'custom_alt' => 1,
          'title' => '',
          'custom_title' => 1,
          'title_type' => 'textfield',
          'default_image' => NULL,
          'use_default_image' => 0,
          'label' => 'Image',
          'weight' => '-1',
          'description' => 'One or more image(s) of the product. The image will automatically be sized appropriately for display in catalog listings and on product pages. The customer will have the opportunity to click on product images to enlarge them for a better look.',
          'type' => 'imagefield_widget',
          'module' => 'imagefield',
        ),
      );
    }

    // Exported field: field_purchase_uri
    $fields[] = array(
      'field_name' => UCAP_FIELD_NAME_PURCHASE_URI,
      'type_name' => UCAP_CONTENT_TYPE_AFFILIATE_PRODUCT,
      'display_settings' => array(
        'weight' => '-2',
        'parent' => '',
        'label' => array(
          'format' => 'hidden',
        ),
        'teaser' => array(
          'format' => 'hidden',
          'exclude' => 0,
        ),
        'full' => array(
          'format' => 'hidden',
          'exclude' => 0,
        ),
        '4' => array(
          'format' => 'default',
          'exclude' => 0,
        ),
        '2' => array(
          'format' => 'default',
          'exclude' => 0,
        ),
        '3' => array(
          'format' => 'default',
          'exclude' => 0,
        ),
        'token' => array(
          'format' => 'default',
          'exclude' => 0,
        ),
      ),
      'widget_active' => '1',
      'type' => 'link',
      'required' => '1',
      'multiple' => '0',
      'module' => 'link',
      'active' => '1',
      'attributes' => array(
        'target' => '_top',
        'rel' => '',
        'class' => '',
      ),
      'display' => array(
        'url_cutoff' => '80',
      ),
      'url' => 0,
      'title' => 'none',
      'title_value' => '',
      'enable_tokens' => 0,
      'widget' => array(
        'default_value' => array(
          '0' => array(
            'title' => '',
            'url' => '',
          ),
        ),
        'default_value_php' => NULL,
        'label' => 'Affiliate Website Product Purchase Address (URI)',
        'weight' => '-2',
        'description' => 'The address of the page on the affiliate website where a customer can purchase this product. If possible, this address should take the customer directly to the product rather than the front page of the affiliate\'s website, to avoid confusing the customer and to increase the chance that the customer will find and buy the product.',
        'type' => 'link',
        'module' => 'link',
      ),
    );

    // Translatables
    array(
      t('Affiliate Website Product Purchase Address (URI)'),
      t('Image'),
    );

    return $fields;
  }