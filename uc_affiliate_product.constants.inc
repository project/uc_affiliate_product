<?php
/**
 * @file Constants used by the Ubercart Affiliate Product module.
 *
 * @author Guy Paddock (guy.paddock@redbottledesign.com).
 */

// Miscellaneous constants
  /**
   * The internal name of this module.
   *
   * @var string
   */
  define('UCAP_MODULE_NAME', 'uc_affiliate_product');

  /**
   * The human-readable name of the content type provided by this module.
   *
   * @var string
   */
  define('UCAP_CONTENT_TYPE_NAME', 'Affiliate Product');

// Permissions
  /**
   * Permission to create affiliate products.
   *
   * @var string
   */
  define('UCAP_PERM_CREATE', 'create ' . strtolower(UCAP_CONTENT_TYPE_NAME) . ' content');

  /**
   * Permission to edit any affiliate products.
   *
   * @var string
   */
  define('UCAP_PERM_EDIT_ANY', 'edit any ' . strtolower(UCAP_CONTENT_TYPE_NAME) . ' content');

  /**
   * Permission to edit the affiliate products the user creates.
   *
   * @var string
   */
  define('UCAP_PERM_EDIT_OWN', 'edit own ' . strtolower(UCAP_CONTENT_TYPE_NAME) . ' content');

  /**
   * Permission to delete any affiliate products.
   *
   * @var string
   */
  define('UCAP_PERM_DELETE_ANY', 'delete any ' . strtolower(UCAP_CONTENT_TYPE_NAME) . ' content');

  /**
   * Permission to delete the affiliate products the user creates.
   *
   * @var string
   */
  define('UCAP_PERM_DELETE_OWN', 'delete own ' . strtolower(UCAP_CONTENT_TYPE_NAME) . ' content');

// Content types
  /**
   * The internal name of the affiliate products content type that this module defines.
   *
   * @var string
   */
  define('UCAP_CONTENT_TYPE_AFFILIATE_PRODUCT', 'product_affiliate');

// CCK Fields
  /**
   * The name of the field that contains the affiliate purchase URL.
   *
   * @var string
   */
  define('UCAP_FIELD_NAME_PURCHASE_URI', 'field_purchase_uri');

  /**
   * The name of the field that contains the product image.
   *
   * @var string
   */
  define('UCAP_FIELD_NAME_IMAGE', 'field_image_cache');

// Form state value keys
  /**
   * The name of the form state value that contains the URI that the user should be directed to when they click on
   * the "Purchase through our partner" link.
   *
   * @var string
   */
  define('UCAP_FORM_STATE_PURCHASE_URI', 'purchase_uri');