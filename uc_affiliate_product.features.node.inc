<?php
/**
 * @file Auto-generated node hook logic created by Features for the Ubercart Affiliate Product module.
 *
 * @author Guy Paddock (guy.paddock@redbottledesign.com).
 */

  /**
   * Helper to implementation of hook_node_info().
   */
  function _uc_affiliate_product_node_info() {
    $items = array(
      'product_affiliate' => array(
        'name' => t(UCAP_CONTENT_TYPE_NAME),
        'module' => UCAP_MODULE_NAME,
        'description' => t('An <em>affiliate product</em> represents a product that is listed on this site, but that must be purchased through an affiliate site.'),
        'has_title' => '1',
        'title_label' => t('Title'),
        'has_body' => '1',
        'body_label' => t('Description'),
        'min_word_count' => '0',
        'help' => '',
      ),
    );
    return $items;
  }
