<?php
/**
 * @file Auto-generated hook logic created by Features for the Ubercart Affiliate Product module.
 *
 * @author Guy Paddock (guy.paddock@redbottledesign.com).
 */

  /**
   * Implementation of hook_content_default_fields().
   */
  function uc_affiliate_product_content_default_fields() {
    module_load_include('inc', UCAP_MODULE_NAME, 'uc_affiliate_product.defaults');
    $args = func_get_args();
    return call_user_func_array('_uc_affiliate_product_content_default_fields', $args);
  }

  /**
   * Implementation of hook_node_info().
   */
  function uc_affiliate_product_node_info() {
    module_load_include('inc', UCAP_MODULE_NAME, 'uc_affiliate_product.features.node');
    $args = func_get_args();
    return call_user_func_array('_uc_affiliate_product_node_info', $args);
  }
